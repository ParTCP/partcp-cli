<?php

/*
	Command-line client for ParTCP servers
	Copyright (C) 2022-2023 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

function show_help(){
	echo <<<EOT

USAGE: partcp <command> [<argument1>[, <argument2>]...]

Commands:
  version - print version information
  list [<server>] - print list of identities [from particular server]
  create_id <identity> - create new identity and generate key pair
  submit_key <identity> <credential> - submit public key for <identity>
  pubkey <identity/server> - print public key of identity or server
  update_server_key <server> - retrieve and save public key from <server>
  ping <server> [<identity>] - send ping message to <server> and print response
  send [<message_file>] - prepare and send message from file or stdin
  send_env <server> <identity> [<message_file>][,<message_file>...] - prepare and send message(s) in envelope from files or stdin
  verify [<message_file>] - verify message signature from file or stdin
  decrypt [<message_file>] - decrypt encrypted fields in message
  help <server> [<message_type>] - request list of supported message types or particular message type definition

<identity> = [<event>+]<participant>@<server>

EOT;
	echo "\n\n";
	exit;
}


function error_exit( $message ){
	$fp = fopen( 'php://stderr', 'w' );
	fwrite( $fp, "{$message}\n" );
	fclose( $fp );
	exit;
}


function create_message( $rawMsg ){
	$data = yaml_parse( $rawMsg );
	if ( empty( $data['To'] ) ){
		return 'Missing \'To\' field in message';
	}
	$remoteId = new ParTCP_Public_Identity( $data['To'], TRUE );
	if ( ! empty( $data['From'] ) ){
		if ( ! strpos( $data['From'], '@' ) ){
			$data['From'] .= '@' . $data['To'];
		}
		$localId = new ParTCP_Private_Identity( $data['From'] );
	}
	$msg = new ParTCP_Outgoing_Message( $remoteId, $localId ?? NULL, $data );
	if ( ! array_key_exists( 'Date', $data ) ){
		$msg->set_date();
	}
	return $msg;
}


#### Main Script ####

error_reporting( E_ALL );
$Version = '0.5';

if ( PHP_SAPI != 'cli' ){
	error_exit('This script must be run from the command line');
}


// Load configuration and libraries

$partcpDir = getenv('PARTCP_CLI_HOME');
if ( ! $partcpDir ){
	$partcpDir = getenv('HOME') . '/.partcp';
}
if ( $argc == 1 ){
	show_help();
}
if ( file_exists( "{$partcpDir}/partcp.conf" ) ){
	$conf = parse_ini_file( "{$partcpDir}/partcp.conf" );
}

require_once __DIR__ . '/lib/partcp-php/crypto.class.php';
require_once __DIR__ . '/lib/partcp-php/incoming_message.class.php';
require_once __DIR__ . '/lib/partcp-php/outgoing_message.class.php';
require_once __DIR__ . '/lib/partcp-php/identity.class.php';
require_once __DIR__ . '/lib/partcp-php/key_storage_fs.class.php';
ParTCP_Key_Storage_Fs::$storageDir = "{$partcpDir}/identities";
ParTCP_Crypto::$useLegacyKx = FALSE;


// Handle command

$command = strtolower( $argv[1] );

if ( $command == 'version' ){
	echo "partcp client {$Version}\n";
	exit;
}

if ( $command == 'pubkey' ){
	if ( empty( $argv[2] ) ){
		show_help();
	}
	$id = new ParTCP_Private_Identity( $argv[2] );
	if ( empty( $id->pubKey ) ){
		$id = new ParTCP_Public_Identity( $argv[2], TRUE );
		if ( empty( $id->pubKey ) ){
			error_exit( "Could not get public key for {$argv[2]}" );
		}
	}
	echo "Public key: {$id->pubKey}\n";
	exit;
}

if ( $command == 'create_id' ){
	if ( empty( $argv[2] ) ){
		show_help();
	}
	$id = new ParTCP_Private_Identity( $argv[2], TRUE, TRUE );
	echo "Generated keypair for {$argv[2]}\n"
		. "Public key: {$id->pubKey}\n";
	exit;
}

if ( $command == 'submit_key' ){
	if ( empty( $argv[3] ) ){
		show_help();
	}
	list ( $name, $server ) = explode( '@', $argv[2] ) + [0,0];
	if ( ! $server ){
		error_exit( "Missing server part in identity." );
	}
	$remoteId = new ParTCP_Public_Identity( $server, TRUE );
	if ( ! $remoteId ){
		error_exit('Could not retrieve the server\'s public key');
	}
	$localId = new ParTCP_Private_Identity( $argv[2] );
	if ( ! $localId ){
		error_exit( "Identity {$argv[2]} does not exist. Use 'create_id' first." );
	}
	$msg = new ParTCP_Outgoing_Message( $remoteId, $localId, [
		'Message-Type' => 'key-submission',
		'Credential~~' => trim( $argv[3] ),
		'Public-Key' => $localId->pubKey,
	]);
	if ( strpos( $name, '+' ) === FALSE ){
		$msg->set_date();
	}
	$response = $msg->send();
	echo "+++ {$response['status']} {$response['message']} +++\n";
	echo trim( $response['body'] ). "\n";
	exit;
}

if ( $command == 'update_server_key' ){
	if ( empty( $argv[2] ) ){
		show_help();
	}
	$id = new ParTCP_Public_Identity( $argv[2], TRUE, TRUE );
	if ( empty( $id->pubKey ) ){
		error_exit('Could not retrieve the server\'s public key');
	}
	echo "Server key updated successfully\n";
	exit;
}

if ( $command == 'list' ){
	$list = ParTCP_Key_Storage_Fs::list_keypairs( $argv[2] ?? NULL );
	foreach ( $list as $id ){
		echo "$id\n";
	}
	exit;
}

if ( $command == 'ping' ){
	if ( empty( $argv[2] ) ){
		show_help();
	}
	$remoteId = new ParTCP_Public_Identity( $argv[2] );
	if ( ! empty( $argv[3] ) ){
		$localId = new ParTCP_Private_Identity( $argv[3] );
		if ( empty( $localId->privKey ) ){
			error_exit( "Identity {$argv[3]} does not exist" );
		}
	}
	$msg = new ParTCP_Outgoing_Message( $remoteId, $localId ?? NULL, 'ping' );
	if ( ! empty( $localId ) ){
		$msg->set( 'Encryption-Request', 'Hello world' );
		$msg->set( 'Decryption-Request', 'Hello world', TRUE );
	}
	$response = $msg->send();
	echo "+++ {$response['status']} {$response['message']} +++\n";
	echo trim( $response['body'] ) . "\n";
	exit;
}

if ( $command == 'send' ){
	$rawMsg = file_get_contents( empty( $argv[2] ) ? 'php://stdin' : $argv[2] );
	if ( strtolower( substr( $rawMsg, 0, 10 ) ) == 'signature:'
		&& strtolower( substr( $rawMsg, 0, 15 ) ) != 'signature: null' ){
		// message is signed, so suppose it's complete and should be sent as is
		if ( ! preg_match( '/^To: (.+)/m', $rawMsg, $matches ) ){
			error_exit("Missing To: element\n");
		}
		$connection = fsockopen( "ssl://{$matches[1]}:443" );
		if ( ! $connection ){
			error_exit("Could not connect to {$matches[1]}\n");
		}
		$request = "POST / HTTP/1.1\r\n"
			. "Host: {$matches[1]}\r\n"
			. "Connection: close\r\n"
			. "Content-Type: application/yaml\r\n"
			. 'Content-Length: ' . strlen( $rawMsg ) . "\r\n"
			. "\r\n"
			. $rawMsg;
		fwrite( $connection, $request );
		$response = '';
		while ( $data = fgets( $connection ) ){
			$response .= $data;
		}
		fclose( $connection );
		$headEnd = strpos( $response, "\r\n\r\n" );
		$head = substr( $response, 0, $headEnd );
		$body = substr( $response, $headEnd + 2 );
		list ( $firstLine, $remainder ) = explode( "\r\n", $head, 2 );
		list ( $dummy, $status, $message ) = explode( ' ', $firstLine, 3 );
		if ( strpos( $remainder, "\r\nTransfer-Encoding: chunked\r\n" ) ) {
			$body = '';
			$markerPos = $headEnd + 4;
			$markerLength = strpos( $response, "\r\n", $markerPos ) - $markerPos;
			$dataSize = hexdec( trim( substr( $response, $markerPos, $markerLength ) ) );
			while ( $dataSize > 0 ) {
				$body .= substr( $response, $markerPos + $markerLength + 2, $dataSize );
				$markerPos =  $markerPos + $markerLength + 2 + $dataSize + 2;
				$markerLength = strpos( $response, "\r\n", $markerPos ) - $markerPos;
				$dataSize = hexdec( trim( substr( $response, $markerPos, $markerLength ) ) );
			}
		}
		$response = compact( 'status', 'message', 'head', 'body' );
	}
	else {
		// message is incomplete and needs completion, signing, encrypting etc.
		$msg = create_message( $rawMsg );
		if ( is_string( $msg ) ){
			error_exit( $msg );
		}
		$noSign = strtolower( substr( $rawMsg, 0, 15 ) ) == 'signature: null';
		$response = $msg->send( $noSign ? FALSE : NULL );
	}
	echo "+++ {$response['status']} {$response['message']} +++\n";
	echo trim( $response['body'] ) . "\n";
	$msg = new ParTCP_Incoming_Message( $response['body'] );
	if ( ! empty( $msg->encryptedElements ) ){
		echo "\n+++ DECRYPTED ELEMENTS +++\n\n";
		foreach ( $msg->encryptedElements as $key => $value ){
			$key = rtrim( $key, '~' );
			echo "{$key}:\n";
			if ( $msg->data[ $key ] ){
				echo is_array( $msg->data[ $key ] )
					? yaml_emit( $msg->data[ $key ], YAML_UTF8_ENCODING )
					: wordwrap( $msg->data[ $key ] ) . "\n";
			}
			else {
				echo "<decryption failed>\n";
			}
		}
	}
	exit;
}

if ( $command == 'send_env' ){
	if ( empty( $argv[2] ) ){
		show_help();
	}
	if ( empty( $argv[4] ) ){
		$messages[] = file_get_contents('php://stdin');
	}
	else {
		$index = 4;
		while ( isset( $argv[ $index ] ) ){
			if ( ! file_exists( $argv[ $index ] ) ){
				error_exit( "File {$argv[ $index ]} not found" );
			}
			$msg = create_message( file_get_contents( $argv[ $index ] ) );
			$messages[] = $msg->dump();
			$index++;
		}
	}
	$remoteId = new ParTCP_Public_Identity( $argv[2] );
	if ( empty( $remoteId->pubKey ) ){
		error_exit("Could not get server key for encryption\n");
	}
	$localId = new ParTCP_Private_Identity( $argv[3], TRUE );
	$msg = new ParTCP_Outgoing_Message( $remoteId, $localId, [
		'To' => $argv[2],
		'Message-Type' => 'envelope',
		'Public-Key' => $localId->pubKey,
		'Content~~' => implode( "\n---\n", $messages )
	]);
	$response = $msg->send();
	echo "+++ {$response['status']} {$response['message']} +++\n";
	echo trim( $response['body'] ) . "\n";
	if ( $response['status'] == 200 ){
		$msg = new ParTCP_Incoming_Message( $response['body'], TRUE );
		if ( isset( $msg->data['Content'] ) ){
			echo "\n+++ DECRYPTED CONTENT +++\n";
			echo wordwrap( $msg->data['Content'] ) . "\n";
		}
	}
	exit;
}


if( $command == 'verify' ){
	$rawMsg =  file_get_contents( empty( $argv[2] ) ? 'php://stdin' : $argv[2] );
	$msg = new ParTCP_Incoming_Message( $rawMsg );
	$result = $msg->get_signature_status();
	echo "---\n";
	echo $result ? "VALID\n" : "INVALID ({$msg->signatureStatusMessage})\n";
	exit;
}

if ( $command == 'decrypt' ) {
	$rawMsg =  file_get_contents( empty( $argv[2] ) ? 'php://stdin' : $argv[2] );
	$msg = new ParTCP_Incoming_Message( $rawMsg );
	echo "---\n" . yaml_emit( $msg->data );
	exit;
}

if ( $command == 'help' && ! empty( $argv[2] ) ){
	$data['Message-Type'] = 'help-request';
	if ( ! empty( $argv[3] ) ){
		$data['Help-Topic'] = $argv[3];
	}
	$msg = new ParTCP_Outgoing_Message( $argv[2], NULL, $data );
	$response = $msg->send();
	if ( ! $response || $response['status'] != 200 ){
		echo "+++ {$response['status']} {$response['message']} +++\n";
		echo trim( $response['body'] ) . "\n";
		exit;
	}
	$data = yaml_parse( $response['body'] );
	if ( ! $data || empty( $data['Help-Content'] ) ){
		error_exit( "+++ Unexpected response +++\n" . $response['body'] );
	}
	$c = $data['Help-Content'];
	if ( empty( $c['Name'] ) ){
		echo "Supported message types:\n- ";
		echo implode( "\n- ", $c ) . "\n";
		exit;
	}
	echo "\n{$c['Name']}\n";
	echo str_repeat( '=', strlen( $c['Name'] ) ) . "\n\n";
	echo "{$c['Purpose']}\n\n";
	echo "Response: {$c['Response']}\n\n";
	echo "Elements:\n";
	echo '  ' . str_replace( "\n", "\n  ", yaml_emit( $c['Elements'] ) ) . "\n";
	if ( ! empty( $c['Notes'] ) ){
		echo "\n" . wordwrap( $c['Notes'] ) . "\n";
	}
	exit;
}

show_help();


// end of file index.php

