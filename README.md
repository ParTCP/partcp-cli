# ParTCP CLI

A command-line client for the ParTCP server written in PHP.


# System requirements

- PHP 8 (CLI) with OpenSSL support and sodium extension


# Installation

1. **Clone git repository to your local disk.**

	````sh
	cd ~/Applications
	git clone --recurse-submodules https://codeberg.org/ParTCP/partcp-cli
	````

2. **Adapt path in *partcp* script file**

	````sh
	nano ~/Applications/partcp-cli/partcp
	````

3. **Move *partcp* script file in your $PATH.**

	Move the *partcp* script file in a directory which is listed in your
	`$PATH` environment variable and make it executable. For example:

	````sh
	mv ~/Applications/partcp-cli/partcp ~/bin
	chmod +x ~/bin/partcp
	````


# Usage

Type `partcp help` to get usage hints.


